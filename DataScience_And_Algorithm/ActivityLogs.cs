﻿using System;
using System.Collections.Generic;

namespace TestWithValence
{
    class ActivityLogs
    {
        static void Main(string[] args)
        {
            PrintResult(LOGS);
            Console.ReadLine();
        }

        static void PrintResult(string logs)
        {
            var result = new Dictionary<string, LogAction>();
            string[] logLines = logs.Split(Environment.NewLine);

            for (int i = 1; i < logLines.Length; i++)
            {
                string[] logLine = logLines[i].Split(",");

                string username = logLine[1].Trim();
                string action = logLine[2].Trim();

                if (!result.ContainsKey(username))
                {
                    LogAction logAction = DecideAction(action, new LogAction());
                    result.Add(username, logAction);
                }
                else
                {
                    result.TryGetValue(username, out LogAction logAction);
                    logAction = DecideAction(action, logAction);
                    result[username] = logAction;
                }
            }

            foreach (var item in result.Keys)
            {
                string response = $"{item}, {result[item].MsgSent}, {result[item].MsgReceived}, {result[item].IsOnline}";
                Console.WriteLine(response);
            }
            /**This is the expected final output:
            *
            *user,messages_sent,messages_received,still_logged_in
           * alice,0,1,true
           * bob,1,2,true
           * carol,1,1,false
           * david,1,2,true
           * esther,2,0,false
           * frank,1,0,false
           */
        }

        public static LogAction DecideAction(string action, LogAction logAction)
        {
            if (logAction != null)
            {
                if (action.Contains("logged_in"))
                {
                    logAction.IsOnline = true;
                }
                else if (action.Contains("logged_out"))
                {
                    logAction.IsOnline = false;
                }

                if (action.Contains("sent_message"))
                {
                    logAction.MsgSent++;
                    logAction.IsOnline = true;
                }
                if (action.Contains("received_message"))
                {
                    logAction.MsgReceived++;
                }

            }

            return logAction;
        }

        public class LogAction
        {
            public int MsgSent { get; set; }
            public int MsgReceived { get; set; }
            public bool IsOnline { get; set; }
        }

        public const string LOGS = @"timestamp,username,event
2019-03-20 00:18:31,alice,logged_in
2019-03-20 01:18:23,bob,sent_message
2019-03-20 01:23:17,alice,received_message
2019-03-20 00:24:08,carol,logged_in
2019-03-20 01:35:09,david,logged_in
2019-03-20 01:36:47,esther,logged_in
2019-03-20 02:02:59,carol,sent_message
2019-03-20 02:18:07,david,received_message
2019-03-20 02:52:37,david,sent_message
2019-03-20 02:53:40,carol,received_message
2019-03-20 03:12:12,esther,sent_message
2019-03-20 03:44:54,bob,received_message
2019-03-20 04:10:15,carol,logged_out
2019-03-20 04:27:16,esther,sent_message
2019-03-20 04:52:17,david,received_message
2019-03-20 05:00:10,esther,logged_out
2019-03-20 06:34:21,frank,logged_in
2019-03-20 07:46:39,frank,sent_message
2019-03-20 07:55:35,bob,received_message
2019-03-20 11:48:31,frank,logged_out";
    }
}
