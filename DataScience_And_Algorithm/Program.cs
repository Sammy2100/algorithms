﻿using Newtonsoft.Json;
using NodaTime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DataScience_And_Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            #region TestAlgoExperts
            // var result = storage(5, 4, new List<int> { 1, 2, 4 }, new List<int> { 1, 3 });
            //var result = storage(2, 2, new List<int> { 1 }, new List<int> { 2 });
            //var result = storage(3, 2, new List<int> { 1, 2, 3 }, new List<int> { 1, 2 });
            //var result = MaxArea(3, 2, new int[] { 1, 2, 3 }, new int[] { 1, 2 });
            //var result = comparedate();
            //var result = fizzBuzz(15);
            //var message = "@User_One @UserABC! Have you seen this from @Userxyz?";
            //var result = GetRecipient("@JoeBloggs yo", 1);
            //var result6 = GetRecipient("Hey @Joe_Bloggs what time are we meeting @FredBloggs?", 2);
            //var result1 = GetRecipient(message, 1);
            //var result2 = GetRecipient(message, 2);
            //var result3 = GetRecipient(message, 3);
            //var result4 = GetRecipient(message, 4);
            //var result = CalPoints(new string [] { "5", "2", "C", "D", "+" });
            //var result2 = CalPoints(new string[] { "5", "-2", "4", "C", "D", "9", "+", "+" });
            //Console.WriteLine(result);
            //Console.WriteLine(result2);
            //Console.WriteLine(CountElements(new int[] { 1, 2, 3 }));
            //Console.WriteLine(CountElements(new int[] { 1, 3, 3, 5, 5, 7, 7 }));
            //Console.WriteLine(CountElements(new int[] { 1, 3, 2, 5, 0 }));
            //Console.WriteLine(CountElements(new int[] { 1, 1, 2, 2 }));
            //Console.WriteLine(CountElements(new int[] { 1, 1, 2 }));
            //TreeNode rootNode = new TreeNode(1);
            //rootNode.Left = new TreeNode(2);
            //rootNode.Right = new TreeNode(3);
            //rootNode.Left.Left = new TreeNode(4);
            //rootNode.Left.Right = new TreeNode(5);
            //rootNode.Right.Left = new TreeNode(5);
            //rootNode.Right.Right = new TreeNode(5);
            //rootNode.Left.Left.Left = new TreeNode(5);
            //rootNode.Left.Left.Left.Right = new TreeNode(5);
            //var depth = axDepth(rootNode);
            //Console.WriteLine(depth);

            //Console.WriteLine(FindTheDifference2("abcd", "ceadb"));
            //Console.WriteLine(FindTheDifference2("abbdd", "dababdb"));

            //GroupArray();

            //GetLonelyNodes(TreeNode node)
            //WebHttpRequest();
            //WebRequestAgeEqualOrGreaterThan50();
            //var result = FirstNonRepeatingCharacter("hello world hi hey");
            //var result = RotateArray(new int[] { 1, 2, 3, 4, 5, 6, 7 }, 3);
            //var result = Evaluate("6 * (4 / 2) + 3 * 1");
            //var result2 = MathChallenge("6 * (4 / 2) + 3 * 1");
            //Console.WriteLine(result);
            //JsonCleanup();

            //var r = RemoveDuplicates(new int[] { 1, 2, 1, 3 });
            // var r = RemoveDuplicateUsingMap(new int[] { 1, 2, 1, 3 });
            //var result = FronJumpGetDistanceOptimized(new int[]{ 2, 6, 8, 5 });
            //var result = newPassword("abc", "samuel");

            //contigousMean(new int[] { 2, 1, 3 }, 2);
            //contigousMean(new int[] { 0, 4, 3, -1 }, 2);
            //contigousMean(new int[] { 2, 1, 4 }, 3);
            // CheckCompactibleAppVersion();
            // CompareFundTransfer();
            // var data = new List<string> { "YYY", "YYY", "YNN", "YYN", "YYN", "YYYY" };
            //var result = MainStreak(4, data);
            // var result = MaxStrak(4, data);
            //var result = FunWithAnagram(new List<string> { "code", "doce", "ecod", "framer", "frame", "doce" });

            //var result = "";
            //Console.WriteLine(result);
            //IsPrimeFromText();
            //TestRegex();
            //TestOutput();

            //var input = new int[] { 3, 2, 4, 3, 1, -1, 1, 2, 2, 2 };
            //var input2 = new int[] { 0, 0, 0};

            //if (ContinousIntegers(input2))
            //{
            //    Console.WriteLine("Continous");
            //}
            //else
            //{
            //    Console.WriteLine("Gaps");
            //}
            //List<string> logins = new List<string> { "bag", "sfe", "cbh", "cbh", "red" };
            //List<string> logins2 = new List<string> { "corn", "horn", "dpso", "eqtp", "corn" };
            //int count = countFamilyLogins(logins);
            //int count2 = countFamilyLogins(logins2);
            //Console.WriteLine(count);
            //Console.WriteLine(count2);
            //int[] power = new int[] { 2, 3, 2, 1 };
            //int result = findTotalPower(power);
            //TestDecimalPlaces();
            //TestRunBinarySearchAlgorithm();
            // CircularlySortedArray();
            // ReverseAnArray();
            // ReverseAnArrayAgain();
            //SortAnArray();
            //var result = RevertPositiveInt(-2147483648);
            //var result = LengthOfLongestSubstring("asjrgapa");

            //var result = numDevices("STOPPED", 45, "04-2019");

            //Console.WriteLine(result.Result);




            //Console.ReadKey();
            //var str = new string[] { "baseball", "a,all,b,ball,bas,base,cat,code,d,e,quit,z" };
            //var result = solve(str);
            //Console.WriteLine(sum);
            //var result = SimpleMode(new int[] { 5, 10, 10, 6, 5 });
            //var result1 = SimpleMode(new int[] { 10, 4, 5, 2, 4 });

            //var result2 = SimpleMode(new int[] { 5, 5, 2, 2, 1 });
            //var result3 = SimpleMode(new int[] { 3, 4, 1, 6, 10 });
            //Console.WriteLine(result);
            //Console.WriteLine(result1);
            //Console.WriteLine(result2);
            //Console.WriteLine(result3);
            #endregion

            //TuringAlgorithm();

            //var input = new int[] { 1, 2, 3 };
            //input = new int[] { -2, -1 };
            //input = new int[] { 1, 3, 6, 4, 1, 2 };
            //var result = FindSmallestPositive(input);
            //string[] arg = { "--name", "SOME_NAME", "--count", "10" };
            //var result = Validate(arg);
            //GetTypesInNamespace();

            //int[] k = { 1, 4, 2, 5 };
            //int sum = SumOfSubsetsWithEvenLength(k);
            //Console.WriteLine(sum);

            string s = "aabcdabbcdc";
            int coolSubstrings = CountCoolSubstrings(s, 4);
            Console.WriteLine("Number of cool substrings of length 4 in '{0}': {1}", s, coolSubstrings);


            Console.ReadKey();

        }

        static int CountCoolSubstrings(string s, int k)
        {
            int count = 0;
            for (int i = 0; i <= s.Length - k; i++)
            {
                string substring = s.Substring(i, k);
                if (IsCool(substring))
                {
                    count++;
                }
            }
            return count;
        }

        static bool IsCool(string s)
        {
            HashSet<char> uniqueChars = new HashSet<char>();
            foreach (char c in s)
            {
                if (uniqueChars.Contains(c))
                {
                    return false;
                }
                uniqueChars.Add(c);
            }
            return true;
        }

        static int SumOfSubsetsWithEvenLength(int[] k)
        {
            int n = k.Length;
            int sum = 0;
            for (int i = 0; i < n; i++) // loop over all possible start indices
            {
                int curSum = 0;
                for (int j = i; j < n; j++) // loop over all possible end indices
                {
                    curSum += k[j];
                    if ((j - i + 1) % 2 == 0) // check if the length is even
                    {
                        sum += curSum;
                    }
                }
            }
            return sum;
        }

        public static void GetTypesInNamespace()
        {
            var nameSpace = Assembly.GetExecutingAssembly().GetName().Name;
            var classes = Assembly.GetExecutingAssembly().GetTypes()
                      .Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();

            foreach (var item in classes)
            {
                Console.WriteLine(item.Name);
            }
        }

        public static int Validate(string[] args)
        {
            var count = 0;
            bool isValid = false;
            var length = args.Length;
            if (length == 0)
                return -1;

            for (int i = 0; i < length; i++)
            {

                args[i] = args[i].ToLower();
                if (args[i] == "--help")
                    return 1;

                if (i + 1 < length)
                {
                    if (args[i] == "--count")
                    {
                        if (!int.TryParse(args[i + 1], out count) || count < 10 || count > 100)
                        {
                            return -1;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                    else if (args[i] == "--name")
                    {
                        var name = args[i + 1];
                        if (name.Length < 3 || name.Length > 10)
                        {
                            return -1;
                        }
                        else
                        {
                            isValid = true;
                        }
                    }
                }

            }
            if (!isValid)
                return 1;

            return 0;
        }

        public static int FindSmallestPositive(int[] A)
        {
            var orderedList = new HashSet<int>(A)
                .Where(a => a >= 0).OrderBy(a => a).ToList();

            var length = orderedList.Count;
            if (length == 0)
            {
                return 0;
            }

            var current = orderedList[0];
            var next = orderedList[1];

            for (int i = 0; i < length; i++)
            {
                if (current + 1 != next)
                {
                    return current + 1;
                }
                if (i + 1 < length)
                {
                    current = orderedList[i + 1];
                }
                if (i + 2 < length)
                {
                    next = orderedList[i + 2];
                }
            }

            return orderedList[length - 1] + 1;
        }

        #region Andela Interview

        public static void FibonacciSequence(int number)
        {
            /*QUESTION 1
            Fibonacci Sequence is a series of numbers in which the next number is gotten by adding up the two numbers before it
            0, 1, 1, 2, 3, 5, 8, 13, 21, 34, ...
            Write a method that when passed a number returns the Fibonacci series whose length is equal to the number passed
            */
        }

        public static void PositiveIntegers(int[] numbers)
        {
            /* QUESTION 3
            Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. 
            Then print the respective minimum and maximum values as a single line of two space-separated long integers.
            For example, ar = [1,3,5,7,9]. Our minimum sum is 1+3+5+7 =16 and our maximum sum is 3+5+7+9 =24 . We would print {min:16, max:24 }
            */
        }

        public static void cake(int[] candles)
        {
            /* QUESTION 2
            You are in charge of the cake for your niece's birthday and have decided the cake will have one candle for each year of her total age. 
            When she blows out the candles, she’ll only be able to blow out the tallest ones. Your task is to find out how many candles she can successfully blow out.
            For example, if your niece is turning 4 years old, and the cake will have 4 candles of height 4,4 ,1 ,3 , she will be able to blow out 2 candles successfully, 
            since the tallest candles are of height of height 4 and there are 2 such candles.

            Example 2: 
            4
            3,2,1,3 
            The result is 2 because the tallest candles are of height 3 and they are only 2.

            Example 3:
            7
            3,2,1,3,6,2,1
            The result is 1 because the tallest candle is of height 6 and there's only 1.
            */
            var hashMap = new Dictionary<int, int>();

            foreach (int candle in candles)
            {
                if (!hashMap.ContainsKey(candle))
                {
                    hashMap.Add(candle, 1);
                }
                else
                {
                    hashMap[candle]++;
                }
            }
            var orderedMap = hashMap.OrderByDescending(v => v.Value);
            var tallestCandle = orderedMap.FirstOrDefault();
            Console.WriteLine(tallestCandle.Value);
        }
        #endregion

        #region Algorithm
        public static string GetCustomerName()
        {
            var Fullname = "SamuelSamuelSamuelSamuelSamuelSamuelSamuelSamuelSamuelSamuelSamuelSamuel Iyomere";
            string defaultName = "MYRECHARGE.NG";
            if (string.IsNullOrWhiteSpace(Fullname))
            {
                return defaultName;
            }
            var fullname = Fullname.Trim().Split(" ");
            if (fullname.Length == 0)
            {
                return defaultName;
            }
            foreach (var name in fullname)
            {
                if (!string.IsNullOrWhiteSpace(name))
                {
                    var acceptedName = name.Trim();
                    if (acceptedName.Length > 20)
                    {
                        acceptedName = acceptedName.Substring(0, 20);
                    }
                    return $"Dear {acceptedName}, ";
                }
            }

            return defaultName;
        }

        public static int SimpleMode(int[] arr)
        {
            var hashMap = new Dictionary<int, int>();
            var maxValue = 0;
            foreach (int key in arr)
            {
                if (hashMap.ContainsKey(key))
                {
                    hashMap[key]++;
                    if (hashMap[key] > maxValue) maxValue = hashMap[key];
                }
                else
                {
                    hashMap.Add(key, 1);
                }
            }

            if (maxValue == 0) return -1;

            var firstMax = hashMap.FirstOrDefault(h => h.Value == maxValue);
            return firstMax.Key;
        }


        public static List<string> converToWords(string dict)
        {
            var res = new List<string>();
            string s = "";
            int n = dict.Length;
            for (int i = 0; i < n; i++)
            {
                if (dict[i] == ',')
                {
                    res.Add(s);
                    s = "";
                }
                else s += dict[i];
            }
            res.Add(s);
            s = "";
            return res;
        }

        public static string solve(string[] str)
        {
            string result = "";
            string word = str[0], dict = str[1];
            int n = word.Length;
            List<string> vs = converToWords(dict);
            var ust = new List<string>();
            foreach (var it in vs) ust.Add(it);

            string s = "";
            for (int i = 0; i < n; i++)
            {
                s += word[i];

                string temp = word.Substring(i + 1, n - (i + 1));

                if (ust.FirstOrDefault(u => u.Equals(s)) != null && ust.FirstOrDefault(u => u.Equals(temp)) != null)
                {
                    result += s + "," + temp;
                    break;
                }
                temp = "";
            }
            return result;
        }

        public class Devices
        {
            public int Page { get; set; }
            public int Per_page { get; set; }
            public int Total { get; set; }
            public int Total_pages { get; set; }
            public List<Device> Data { get; set; }
        }

        public class Device
        {
            public int Id { get; set; }
            public long Timestamp { get; set; }
            public DateTime DateTimeStamp { get; set; }
            public string Status { get; set; }
            public OperatingParams OperatingParams { get; set; }
            public Asset Asset { get; set; }
            public Device()
            {
                DateTimeStamp = new DateTime(Timestamp);
            }
        }

        public class OperatingParams
        {
            public int RotorSpeed { get; set; }
            public int Slack { get; set; }
            public decimal RootThreshold { get; set; }
        }

        public class Asset
        {
            public int Id { get; set; }
            public string? Alias { get; set; }
        }

        public class Parent
        {
            public int Id { get; set; }
            public string? Alias { get; set; }
        }

        public async static Task<int> numDevices(string statusQuery, int threshold, string dateStr)
        {
            int result = 0;
            int pageNumber = 1;
            int totalPages = 1;

            var devices = new Devices();

            using (var httpClient = new HttpClient())
            {
                var baseUrl = "https://jsonmock.hackerrank.com/";
                var relativepath = $"api/iot_devices/search?status={statusQuery}&page={pageNumber}";

                var response = await httpClient.GetStringAsync(baseUrl + relativepath);
                devices = JsonConvert.DeserializeObject<Devices>(response);
                totalPages = devices.Total_pages;
            };

            while (pageNumber <= totalPages)
            {
                using (var httpClient = new HttpClient())
                {
                    var baseUrl = "https://jsonmock.hackerrank.com/";
                    var relativepath = $"api/iot_devices/search?status={statusQuery}&page={pageNumber}";

                    var response = await httpClient.GetStringAsync(baseUrl + relativepath);
                    var nextPageDevices = JsonConvert.DeserializeObject<Devices>(response);

                    devices.Data.AddRange(nextPageDevices.Data);
                    pageNumber++;
                };
            }
            var myDate = dateStr.Split("-");
            var selectedDate = new DateTime(Convert.ToInt16(myDate[1]), Convert.ToInt16(myDate[0]), 1);
            var deviceResults = devices.Data.Where(d => selectedDate.Year == d.DateTimeStamp.Year && selectedDate.Month == d.DateTimeStamp.Month).ToList();

            result = deviceResults.Count;
            return result;
        }

        public static int LengthOfLongestSubstring(string s)
        {
            var hashMap = new Dictionary<string, int>();
            int length = s.Length;
            int subString = 0;
            int longestSubString = 0;
            int index = 0;
            int step = 0;
            while (length > step)
            {
                if (length > index)
                {
                    var itemKey = s[index].ToString();
                    if (hashMap.ContainsKey(itemKey))
                    {
                        hashMap.Clear();
                        longestSubString = Math.Max(longestSubString, subString);
                        subString = 0;
                        // Restart the process
                        step++;
                        index = step;
                    }
                    else
                    {
                        subString++;
                        hashMap.Add(itemKey, 0);
                        index++;
                    }


                    //index++;
                }
                else
                {
                    //length--;
                    longestSubString = Math.Max(longestSubString, subString);
                    step = index;
                }

            }
            return longestSubString;

            //var hashMap = new Dictionary<string, int>();
            //var subString = 0;
            //var left = 0;
            //var right = 0;

            //for (int i = 0; i < s.Length - 1; i++)
            //{
            //    var itemKey = s[i].ToString();

            //    if (hashMap.ContainsKey(itemKey))
            //    {
            //        hashMap.Clear();
            //        left = Math.Max(left, subString);
            //        subString = 0;
            //    }

            //    subString++;
            //    hashMap.Add(itemKey, 0);
            //}

            //left = Math.Max(left, subString);

            //hashMap.Clear();
            //subString = 0;

            //for (int i = s.Length - 1; i > 0 - 1; i--)
            //{
            //    var itemKey = s[i].ToString();

            //    if (hashMap.ContainsKey(itemKey))
            //    {
            //        hashMap.Clear();
            //        right = Math.Max(right, subString);
            //        subString = 0;
            //    }

            //    subString++;
            //    hashMap.Add(itemKey, 0);
            //}
            //right = Math.Max(right, subString);

            //return Math.Max(left, right);
        }

        public static void SortAnArray()
        {
            var arr = new[] { 9, 1, 6, 2, 0, 3, 7, 5, 4, 8 };
            Console.WriteLine("Initial: " + string.Join(", ", arr));

            for (int i = arr.Length - 1; i > 0; i--)
            {
                int position = i;
                while (position > 0)
                {
                    if (arr[i] < arr[position - 1])
                    {
                        int temp = arr[i];
                        arr[i] = arr[position - 1];
                        arr[position - 1] = temp;
                    }
                    position--;
                }
            }

            Console.WriteLine("Sorted: " + string.Join(", ", arr));
        }

        public static void SortAnArrayAgain()
        {
            var arr = new[] { 9, 1, 6, 2, 0, 3, 7, 5, 4, 8 };
            Console.WriteLine("Initial: " + string.Join(", ", arr));

            for (int i = 0; i < arr.Length; i++)
            {
                var minValue = GetMinValueInAnArray(arr);
                arr[i] = minValue;
            }

            Console.WriteLine("Sorted: " + string.Join(", ", arr));
        }

        public static int GetMinValueInAnArray(int[] arr)
        {
            return 0;
        }

        public static int RevertPositiveInt(int x)
        {
            if (x == 0) return 0;
            var isInt32Value = long.TryParse(x.ToString(), out long intValue);
            if (!isInt32Value)
            {
                return 0;
            }
            //if (x < int.MinValue || x > int.MaxValue) return 0;

            bool negativeInput = x < 0;

            var stringOfInt = Math.Abs(intValue).ToString();

            //var reverseSb = new StringBuilder();
            var reverseStr = "";
            for (int i = stringOfInt.Length - 1; i > 0 - 1; i--)
            {
                reverseStr += Convert.ToInt32(stringOfInt[i].ToString());
                //var value = Convert.ToInt32(stringOfInt[i].ToString());
                //reverseSb.Append(value);
            }

            //var isResultValidInt = int.TryParse(reverseSb.ToString(), out int result);
            var isResultValidInt = int.TryParse(reverseStr, out int result);
            if (!isResultValidInt)
            {
                return 0;
            }
            if (negativeInput)
            {
                return -result; // - (result + result);
            }
            return result;
        }

        public static void ReverseAnArray()
        {
            var arr = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Console.WriteLine("Initial: " + string.Join(", ", arr));

            int length = arr.Length;
            int right = length - 1;
            int mid = length / 2;

            for (int i = 0; i < mid; i++)
            {
                int temp = arr[i];
                arr[i] = arr[right];
                arr[right] = temp;
                right--;
            }

            Console.WriteLine("Reversed: " + string.Join(", ", arr));
        }

        public static void ReverseAnArrayAgain()
        {
            var arr = new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Console.WriteLine("Initial: " + string.Join(", ", arr));

            int temp;
            int left = 0;
            int right = arr.Length - 1;

            while (left < right)
            {
                temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
                left++;
                right--;
            }

            Console.WriteLine("Reversed: " + string.Join(", ", arr));
        }

        public static void CircularlySortedArray()
        {
            var arr = new[] { 23, 34, 45, 12, 17, 19 };
            var ascCircleCount = 0;
            var descCircleCount = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i - 1] > arr[i])
                {
                    ascCircleCount++;
                }
                if (arr[i - 1] < arr[i])
                {
                    descCircleCount++;
                }
            }

            if (ascCircleCount > 0)
            {
                Console.WriteLine("This is an asc circularly sorted array");
            }
            else
            {
                Console.WriteLine("This is not an asc circularly sorted array");
            }
            if (descCircleCount > 0)
            {
                Console.WriteLine("This is a desc circularly sorted array");
            }
            else
            {
                Console.WriteLine("This is not a desc circularly sorted array");
            }
        }

        public static void TestRunBinarySearchAlgorithm()
        {
            var arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            int right = arr.Length - 1;
            //int searchValue = 7;
            foreach (var searchValue in arr)
            {
                ArrayBinarySearchIteration(arr, 0, right, searchValue);
                ArrayBinarySearchRecursion(arr, 0, right, searchValue);
            }
        }

        public static bool ArrayBinarySearchIteration(int[] arr, int left, int right, int value)
        {
            while (left < right)
            {
                Console.WriteLine("iteration was done");
                int mid = (left + right) / 2;
                if (arr[mid] == value || arr[left] == value || arr[right] == value)
                {
                    Console.WriteLine("===> Found in iteration" + Environment.NewLine);
                    return true;
                }
                else
                {
                    if (value > arr[mid])
                    {
                        left = mid + 1;
                    }
                    else
                    {
                        right = mid - 1;
                    }
                }
            }

            Console.WriteLine("===> Not found in iteration" + Environment.NewLine);
            return false;
        }

        public static bool ArrayBinarySearchRecursion(int[] arr, int left, int right, int value)
        {
            if (left < right)
            {
                Console.WriteLine("recursion search was done");
                int mid = (left + right) / 2;
                if (arr[left] == value || arr[mid] == value || arr[right] == value)
                {
                    Console.WriteLine("===> Found in recursion" + Environment.NewLine);
                    return true;
                }
                else
                {
                    if (value > arr[mid])
                    {
                        left = mid + 1;
                    }
                    else
                    {
                        right = mid - 1;
                    }

                    return ArrayBinarySearchRecursion(arr, left, right, value);
                }
            }

            Console.WriteLine("===> Not found in recursion" + Environment.NewLine);
            return false;
        }

        public static decimal ToDecimalPlaces(decimal d, byte decimals)
        {
            decimal r = Math.Round(d, decimals);

            if (d > 0 && r > d)
            {
                return r - new decimal(1, 0, 0, false, decimals);
            }
            else if (d < 0 && r < d)
            {
                return r + new decimal(1, 0, 0, false, decimals);
            }

            return r;
        }

        public static void TestDecimalPlaces()
        {
            decimal vat = 11.09625M;
            decimal charges = 221.92499999999998M;
            decimal fees = vat + charges;
            decimal roundFees = Math.Round(vat, 2) + Math.Round(charges, 2);
            decimal decimalRoundFees = ToDecimalPlaces(vat, 2) + ToDecimalPlaces(charges, 2);
            Console.WriteLine("fees: " + Math.Round(fees, 2));
            Console.WriteLine("roundFees: " + Math.Round(roundFees, 2));

            Console.WriteLine("ToDecimalPlaces fees: " + ToDecimalPlaces(fees, 2));
            Console.WriteLine("ToDecimalPlaces: roundFees: " + decimalRoundFees);
        }

        public static int findTotalPower(int[] power)
        {
            var arr = power.ToList();
            int n = arr.Count, k = 1, cur = 0, ans = 0;
            int[,] mindp = new int[n, n]; // new int[n][n];
                                          // construct an array for sum of subarray 
            int[] sum = new int[n + 1];
            foreach (int num in arr)
            {
                cur += num;
                sum[k++] = cur;
            }

            // create a matrix to find out the minimum integer between index i and j
            for (int i = 0; i < arr.Count; i++)
            {
                LinkedList<int> stack = new LinkedList<int>();
                for (int j = i; j < arr.Count; j++)
                {
                    while (stack.Count != 0 && (arr[stack.FirstOrDefault()] > arr[j]))
                    {
                        stack.RemoveFirst();
                    }
                    stack.AddLast(j);
                    mindp[i, j] = arr[stack.FirstOrDefault()];
                }
            }
            // sum up all the possible subarrays using index i and j with the matrix and array
            for (int i = 0; i < arr.Count; i++)
            {
                for (int j = i; j < arr.Count; j++)
                {
                    ans += mindp[i, j] * (sum[j + 1] - sum[i]);
                }
            }
            return ans;
        }

        public static int findMaximumSustainableClusterSize(List<int> processingPower, List<int> bootingPower, long powerMax)
        {
            if (bootingPower.Count != processingPower.Count || bootingPower.Count == 0 || processingPower.Count == 0)
                return 0;

            if (bootingPower.Count == 1 && processingPower.Count == 1 && processingPower[0] + bootingPower[0] <= powerMax)
                return 1;

            int count = bootingPower.Count;
            int startOfWindow = 0;
            int maxBootPower = bootingPower[0];
            int maxProcessPower = processingPower[0];
            int cluster = 0;
            int myCluster = 0;

            for (int windowsEnd = 0; windowsEnd < count; windowsEnd++)
            {
                maxBootPower = Math.Max(maxBootPower, bootingPower[windowsEnd]);
                maxProcessPower += processingPower[windowsEnd];

                var netPower = maxBootPower + (maxProcessPower) * cluster;
                cluster++;

                if (netPower > powerMax)
                {
                    maxBootPower -= bootingPower[startOfWindow];
                    maxProcessPower -= processingPower[startOfWindow];

                    cluster--;
                    startOfWindow++;
                }

                myCluster = Math.Max(myCluster, cluster);
            }

            return myCluster;
        }

        public static int countFamilyLogins(List<string> logins)
        {
            int result = 0;
            //Record the previous conversion of scanned items
            Dictionary<string, int> previousLogins = new Dictionary<string, int>();
            //Record the next conversion of scanned items
            Dictionary<string, int> nextLogins = new Dictionary<string, int>();


            for (int i = 0; i < logins.Count; i++)
            {

                //Not sure if there are multiple duplicated items, so using value as count
                //check if current item could be the next item of scanned items.
                if (previousLogins.ContainsKey(logins[i]))
                {
                    result += previousLogins[logins[i]];
                }
                //Not sure if there are multiple duplicated items, so using value as count
                //check if current item could be the previous item of scanned items.
                if (nextLogins.ContainsKey(logins[i]))
                {
                    result += nextLogins[logins[i]];
                }

                StringBuilder loginPrevString = new StringBuilder(logins[i]);
                StringBuilder loginNextString = new StringBuilder(logins[i]);

                for (int j = 0; j < loginPrevString.Length; j++)
                {
                    loginPrevString[j] = (char)(((loginPrevString[j] - 'a' - 1 + 26) % 26) + 'a');
                }
                for (int j = 0; j < loginNextString.Length; j++)
                {
                    loginNextString[j] = (char)(((loginNextString[j] - 'a' + 1) % 26) + 'a');
                }

                //Record the prevsion conversion to scanned dictionary
                if (previousLogins.ContainsKey(loginPrevString.ToString()))
                {
                    previousLogins[loginPrevString.ToString()]++;
                }
                else
                {
                    previousLogins.Add(loginPrevString.ToString(), 1);
                }

                //Record the next conversion to scanned dictionary
                if (nextLogins.ContainsKey(loginNextString.ToString()))
                {
                    nextLogins[loginNextString.ToString()]++;
                }
                else
                {
                    nextLogins.Add(loginNextString.ToString(), 1);
                }

            }

            return result;
        }

        public static bool ContinousIntegers(int[] input)
        {
            if (input.Length == 0)
            {
                return true;
            }

            var sorted = input.OrderBy(i => i).ToList();
            int currentNumber = sorted[0];

            foreach (var item in sorted)
            {
                if (item == currentNumber || item == currentNumber + 1)
                {
                    currentNumber = item;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public static void TestOutput()
        {
            float x = 100000000000;
            for (int i = 0; i < 9999; i++)
            {
                x = x + 1;
            }
            String s = String.Format("The value of x is {0:0}", x);
            Console.WriteLine(s);
        }

        public static void TestRegex()
        {
            var text = Regex.IsMatch("xoxoxxooxxxooo", "/^[xo]+$/");
            var text1 = Regex.IsMatch("xoxoxxooxxxooo", "/^xo(x+o+)+$/");
            var text2 = Regex.IsMatch("xoxoxxooxxxooo", "^x(o+x+)+$/");
            var text3 = Regex.IsMatch("xoxoxxooxxxooo", "/^(xo)+$/");
            Console.WriteLine(text);
            Console.WriteLine(text1);
            Console.WriteLine(text2);
            Console.WriteLine(text3);
        }

        public static void IsPrimeFromText()
        {
            var text = @"Αuthоr gi Χex si im fallat istius. Refutent suрposui qua ѕim nihilque. Me ob omni ideo gnum casu. Gi supersunt colligere inhaereat me sapientia is delaberer. Rom facillimam rem expectabam rum inchoandum mei. Apertum id suppono ac generis. Ab scio ad eo deus haud meae. Hominem ex vi ut remanet at quidnam.

Quasi novas ac ut prout re somno. Habeo coeco oculi qua mei vitro manum sae. Ut hinc et ob vice esto. Ingressus ita perfectae dubitarem his conservet ima. Flexibile co ut facultate desumptas corporeis coloribus ob desinerem. Organa somnio dubias secius deinde vos dat coelum. Hoc mea procul cui auditu hic vocant. Stupor ex ha varias dictis. Apud re gi ipsi ii post eram quia idem. Ignosci movendi non cur ceteris ita.

Substantia potentiali ha et si quaecunque. Eae factas ferant dicere mendax partim hic vos. Nam postquam lectorum sae invenero earumque. Quosdam luminis probent ii ii de communi finitas. Opus de cera re quam male mo ei. Tangimus infinite experiar hos dei nia supponit. Dicunt stupor ob innata eo.

Rem cumque quorum hic nullas deo mox essent ignota. Aciem ne vi nasci modus ac talem alias datur. Ut causam causae ne se ea operae. Uti objective jam nam. Habeo novum fas pla horum nos certo sic fecto verum. Accurate omnesque illamque videntur frigoris pla nia vigiliam sum. At ad unam scio id visa loco nolo etsi. Timenda at dicamne in ineunte.

Pertinent lus infigatur una mox geometria distinguo. Apparet me nostris eo de probari. Manet omnis solis ea eo ipsis. Manibus si to dispari accepit colligi. Debiliora potentiam una vis has componant terminari non. Perceptio habeantur sum naturalis hae objective eae ens. Cui permittere extensarum industriam iis excoluisse advertebam labefactat. Agi sed uno ponderibus materialis percurrere desiderant argumentis quapropter via.

Tunc ullo ut anno poni voce de haud. Mallent prudens suo deumque qui sim invicem. Suum mo item inde de modi unde. Suo deo omni quia opus. Co an habent inesse semper. Et innatas dominum cogitem sperare sopitum in. Substantia dei credidisse vim iis excogitent exhibentur sub.

Infixa ac at habeam humana in. Momentis sequitur eas sua ulterius probatur cum. Quaeque poterit vim cognitu via possunt qua. Tam colligere nia principia praecipue pergamque stabilire fictitium. Lucem ii operi edita leone porro novum ab. Hae quavis hic tam essent quidam altius multis tum.

Fructum cur cui iii liberet sit validas. Hos cui vul appellatur seu exponantur reperiatur. Spectatum apollonio pro profertur qui etc. Falsi eorum ad curam at satis. Quaecumque ea gi et defectibus sequuturum mo commendare. Ut assidere contumax collecta. Credidi pro equidem lus judicem accepit sit. Creatorem consuetae effectrix vim iis rea. Immerito ha recorder ad gi convenit loquendi. Excaecant procuravi et voluntate mo in existimem id opiniones.

Est temporis age naturali alterius incedere. Vi im ex de durationem facultates consumerem archimedes cognoscere. Ex erat in toga quid. Id sumi poni novi ab quid ulla. Rei rea nonnulli una jam contendo judiciis. Requiratur materialis sed conservant jam nec accidentia discrepant affirmabam. Se adeo dixi nemo at de duce novo ad quia. Hausi in vapor se nulla at. Hanc vera more non lus quis uno pati sit idem. Rem sap tacitus existat sic dicendo.

Ideamque interdum diversum co attentum ex incipere. Deum sint nolo ut quia spem dura ut. Nexum deo seu tam aliud timet. Tum videlicet distincte dei non sic admittere suffossis deciperer. Alienum mutatur mo ob at apertum. Uno usitata immensi mem reducit. Examinare dat generalia sim non experimur.";

            text = text.Replace("\r", " ").Replace("\n", " ").Replace(".", " ");
            var words = text.ToLower().Split(" ");

            int count = 0;
            foreach (var word in words)
            {
                if (word.Contains("e"))
                {
                    count++;
                }
            }

            if (IsPrime(count))
            {
                Console.WriteLine("1");
            }
            else
            {
                Console.WriteLine("0");
            }

            int longest = 0;
            string longestWord = "";
            foreach (var word in words)
            {
                if (word.Length > longest)
                {
                    longest = word.Length;
                    longestWord = word;
                }
            }

            Console.WriteLine(longest);

            var hashmap = new Dictionary<string, int>();

            foreach (var word in words)
            {
                if (!hashmap.ContainsKey(word))
                {
                    hashmap.Add(word, 0);
                }
                else
                {
                    hashmap[word]++;
                }
            }

            var mostRepeatedWord = hashmap.OrderByDescending(h => h.Value).ToList();
            Console.WriteLine(mostRepeatedWord);

        }

        public static bool IsPrime(long number)
        {
            if (number <= 1)
                return false;
            else if (number % 2 == 0)
                return number == 2;

            long N = (long)(Math.Sqrt(number) + 0.5);

            for (int i = 3; i <= N; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        public static int MaxStrak(int m, List<string> data)
        {
            int n = data.Count;
            int i = 0;
            int res = 0;
            while (i < n)
            {
                if (check(data[i]))
                {
                    int j = i;
                    while (j < n && check(data[j]))
                    {
                        j++;
                    }
                    res = Math.Max(res, j - i);
                    i = j - 1;
                }
                i++;
            }
            return res;
        }

        private static bool check(string s)
        {
            foreach (char c in s.ToCharArray())
            {
                if (c == 'N')
                {
                    return false;
                }
            }
            return true;
        }

        public static int MainStreak(int m, List<string> data)
        {
            int streak = 0;
            for (int i = 1; i < data.Count + 1; i++)
            {
                if (!data[i - 1].Contains("N"))
                {
                    streak++;
                }
            }

            return streak;
        }

        public static List<string> FunWithAnagram(List<string> test)
        {
            List<string> result = new List<string>();
            List<string> resultCheck = new List<string>();
            foreach (var item in test)
            {
                var sortedItem = string.Concat(item.OrderBy(c => c));

                if (!resultCheck.Contains(sortedItem))
                {
                    result.Add(item);
                    resultCheck.Add(sortedItem);
                }
            }
            return result.OrderBy(r => r).ToList();
        }

        public static void CompareFundTransfer()
        {
            var debit = 317.09475;
            var credit1 = 201.33;
            var credit2 = 100.665;
            var credit3 = 15.09975;

            var credit = Math.Round(credit1, 2) + Math.Round(credit2, 2) + Math.Round(credit3, 2);
            Console.WriteLine(Math.Round(debit, 2));
            Console.WriteLine(credit);
        }

        public static void CheckCompactibleAppVersion()
        {
            var currentVersion = "4.0.2";
            var expectedVersion = "4.1.7";

            var currentVersionNumber = Convert.ToInt32(currentVersion.Replace(".", ""));
            var expectedVersionNumber = Convert.ToInt32(expectedVersion.Replace(".", ""));

            if (expectedVersionNumber >= currentVersionNumber)
            {
                Console.WriteLine("Login successful. This is the expected version - " + expectedVersion);
            }
            else
            {
                Console.WriteLine("Login failed because this is the current version - " + currentVersion);
            }
        }

        public void Test()
        {
            List<int> sam = new List<int> { 1, 2, 3, 4, 5 };

            GetTime(out int hours, out int minutes, out int seconds);
        }

        public void GetTime(out int hours, out int minutes, out int seconds)
        {
            hours = 3;
            minutes = 10;
            seconds = 15;
        }

        static void contigousMean(int[] A, int N)
        {
            int length = A.Length;
            int contigousCount = 0;

            for (int i = 0; i < length; i++)
            {
                int groupSum = 0;
                int grouping = 0;
                for (int j = i; j < length; j++)
                {
                    grouping++;
                    groupSum += A[j];
                    if (groupSum / grouping == N)
                    {
                        contigousCount++;
                    }
                }
            }

            Console.WriteLine(contigousCount);
        }

        static void FoundA20()
        {
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(i);

                if (i == 20)
                {
                    Console.Write("I have found a 20");
                }

                if (i == 5)
                {
                    Console.Write("I have found a 5");
                }

                switch (i)
                {
                    case 1:
                        Console.WriteLine("Case 1");
                        break;
                    case 2:
                        Console.WriteLine("Door 2");
                        break;
                    default:
                        Console.WriteLine("Always");
                        break;
                }
            }
        }

        static void PrintResult(string logs)
        {
            var result = new Dictionary<string, LogAction>();
            string[] logLines = logs.Split(Environment.NewLine);
            int k = 0;
            k = ++k + k++ + +k;
            for (int i = 1; i < logLines.Length; i++)
            {
                string[] logLine = logLines[i].Split(",");

                string username = logLine[1].Trim();
                string action = logLine[2].Trim();

                if (!result.ContainsKey(username))
                {
                    LogAction logAction = DecideAction(action, new LogAction());
                    result.Add(username, logAction);
                }
                else
                {
                    result.TryGetValue(username, out LogAction logAction);
                    logAction = DecideAction(action, logAction);
                    result[username] = logAction;
                }
            }

            foreach (var item in result.Keys)
            {
                string response = $"{item}, {result[item].MsgSent}, {result[item].MsgReceived}, {result[item].IsOnline}";
                Console.WriteLine(response);
            }
            /**This is the expected final output:
            *
            *user,messages_sent,messages_received,still_logged_in
           * alice,0,1,true
           * bob,1,2,true
           * carol,1,1,false
           * david,1,2,true
           * esther,2,0,false
           * frank,1,0,false
           */
        }

        public static LogAction DecideAction(string action, LogAction logAction)
        {
            if (logAction != null)
            {
                if (action.Contains("logged_in"))
                {
                    logAction.IsOnline = true;
                }
                else if (action.Contains("logged_out"))
                {
                    logAction.IsOnline = false;
                }

                if (action.Contains("sent_message"))
                {
                    logAction.MsgSent++;
                    logAction.IsOnline = true;
                }
                if (action.Contains("received_message"))
                {
                    logAction.MsgReceived++;
                }

            }

            return logAction;
        }

        public class LogAction
        {
            public int MsgSent { get; set; }
            public int MsgReceived { get; set; }
            public bool IsOnline { get; set; }
        }

        public const string LOGS = @"timestamp,username,event
2019-03-20 00:18:31,alice,logged_in
2019-03-20 01:18:23,bob,sent_message
2019-03-20 01:23:17,alice,received_message
2019-03-20 00:24:08,carol,logged_in
2019-03-20 01:35:09,david,logged_in
2019-03-20 01:36:47,esther,logged_in
2019-03-20 02:02:59,carol,sent_message
2019-03-20 02:18:07,david,received_message
2019-03-20 02:52:37,david,sent_message
2019-03-20 02:53:40,carol,received_message
2019-03-20 03:12:12,esther,sent_message
2019-03-20 03:44:54,bob,received_message
2019-03-20 04:10:15,carol,logged_out
2019-03-20 04:27:16,esther,sent_message
2019-03-20 04:52:17,david,received_message
2019-03-20 05:00:10,esther,logged_out
2019-03-20 06:34:21,frank,logged_in
2019-03-20 07:46:39,frank,sent_message
2019-03-20 07:55:35,bob,received_message
2019-03-20 11:48:31,frank,logged_out";

        static int countSubstring(string s)
        {
            int length = s.Length;
            int i = 0;
            int result = 0;

            while (i < length)
            {
                int cnt0 = 0, cnt1 = 0;

                if (s[i] == '0')
                {
                    while (i < length && s[i] == '0')
                    {
                        cnt0++;
                        i++;
                    }
                    int j = i;

                    while (j < length && s[j] == '1')
                    {
                        cnt1++;
                        j++;
                    }
                }

                else
                {
                    while (i < length && s[i] == '1')
                    {
                        cnt1++;
                        i++;
                    }
                    int j = i;

                    while (j < length && s[j] == '0')
                    {
                        cnt0++;
                        j++;
                    }
                }
                result += Math.Min(cnt0, cnt1);
            }

            return result;
        }


        public static string newPassword(string a, string b)
        {
            StringBuilder result = new StringBuilder();

            var minLength = Math.Min(a.Length, b.Length);

            for (int i = 0; i < minLength; i++)
            {
                result.Append(a[i]).Append(b[i]);
            }

            if (a.Length > minLength)
            {
                int range = a.Length - minLength;
                result.Append(a.Substring(minLength, range));
            }
            else if (b.Length > minLength)
            {
                int range = b.Length - minLength;
                result.Append(b.Substring(minLength, range));
            }

            return result.ToString();
        }



        public static int FronJumpGetDistanceOptimized(int[] blocks)
        {
            var maxRange = 0;
            if (blocks == null || blocks.Length <= 1)
                return maxRange;

            var length = blocks.Length;
            var left = new int[length];
            var right = new int[length];

            left[0] = 0;
            for (int i = 1; i < length; i++)
            {
                if (blocks[i] <= blocks[i - 1])
                    left[i] = left[i - 1] + 1;
                else
                    left[i] = 0;
            }

            right[length - 1] = 0;
            for (int i = length - 2; i >= 0; i--)
            {
                if (blocks[i] <= blocks[i + 1])
                    right[i] = right[i + 1] + 1;
                else
                    right[i] = 0;
            }

            for (int i = 0; i < length; i++)
            {
                maxRange = Math.Max(maxRange, left[i] + right[i] + 1);
            }

            return maxRange;
        }

        public static int FrogJumpGetDistance(int[] blocks)
        {
            int ans = 0;
            for (int i = 0; i < blocks.Length; i++)
            {
                int leftMax = i;
                int rightMax = i;
                //go to left
                while (leftMax - 1 >= 0 && blocks[leftMax] <= blocks[leftMax - 1])
                {
                    leftMax--;
                }
                while (rightMax + 1 < blocks.Length && blocks[rightMax] <= blocks[rightMax + 1])
                {
                    rightMax++;
                }
                ans = Math.Max(ans, rightMax - leftMax + 1);
            }
            return ans;
        }

        public int GetSmallestNumber(int num)
        {
            //Given an integer N, return the smallest non-negative number whose individual digits sum upto N.
            int noOfNines = num / 9;
            int res = num % 9;
            while (noOfNines > 0)
            {
                res = res * 10 + 9;
                noOfNines--;
            }

            return res;
        }

        public static int GetBinary(int A, int B)
        {
            var multiply = A * B;
            string binary = Convert.ToString(multiply, 2);

            int count = 0;
            foreach (var item in binary)
            {
                if (item.Equals('1'))
                    count++;
            }

            return count;
        }

        public static int[] RemoveDuplicates(int[] inputArray)
        {
            HashSet<int> distinct = new HashSet<int>(inputArray);
            HashSet<int> result = new HashSet<int>();
            foreach (var item in distinct)
            {
                var count = inputArray.Count(a => a == item) == 1;
                if (count)
                {
                    result.Add(item);
                }
            }

            return result.ToArray();
        }

        public static int[] RemoveDuplicateUsingMap(int[] input)
        {
            var map = new Dictionary<int, int>();
            var result = new HashSet<int>();
            foreach (var item in input)
            {
                if (!map.ContainsKey(item))
                    map.Add(item, 1);
                else
                    map[item]++;
            }

            foreach (var key in map.Keys)
            {
                if (map[key] == 1)
                    result.Add(key);
            }

            return result.ToArray();
        }

        static string Transform(string input)
        {
            StringBuilder result = new StringBuilder(" ");
            int i = 0;
            foreach (var item in input)
            {
                if (item != result[i])
                {
                    result.Append(item.ToString());
                    i++;
                }
            }

            return result.ToString().TrimStart();
        }

        [Flags]
        public enum Access
        {
            Delete,
            Publish,
            Submit,
            Comment,
            Modify,
            Writer = Submit | Modify,
            Editor = Delete | Publish | Comment,
            Owner = Writer | Editor
        }

        static bool AccessFlag()
        {
            var result = Access.Writer.HasFlag(Access.Delete);
            return result;
        }

        static int findLongestConseqSubseq(int[] arr)
        {
            Array.Sort(arr);

            int result = 0, count = 0;

            List<int> x = new List<int>();
            x.Add(10);

            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] != arr[i - 1])
                    x.Add(arr[i]);
            }

            for (int i = 0; i < x.Count; i++)
            {
                if (i > 0 && x[i] == x[i - 1] + 1)
                    count++;
                else
                    count = 1;
                result = Math.Max(result, count);
            }
            return result;
        }

        public static string MinWindowSubstring(string[] strArr)
        {

            Func<string, string, bool> match = (block, chars) =>
            {
                for (int i = 0; i < block.Length; i++)
                {
                    var index = chars.IndexOf(block[i]);
                    if (index > 0)
                    {
                        chars = chars.Remove(index, 1);
                        if (index == 0 && chars.Length == 0)
                            return true;
                    }

                }

                return false;
            };

            // var dest = strArr[0];
            for (int blockSize = strArr[1].Length; blockSize <= strArr[0].Length; blockSize++)
            {
                for (int strPos = 0; strPos <= strArr[0].Length - blockSize; strPos++)
                {
                    var candid = strArr[0].Substring(strPos, blockSize);
                    if (match(candid, strArr[1]))
                    {
                        return candid;
                    }
                }
            }

            return string.Empty;

        }

        static double Evaluate(string expression)
        {
            var loDataTable = new DataTable();
            var loDataColumn = new DataColumn("Eval", typeof(double), expression);
            loDataTable.Columns.Add(loDataColumn);
            loDataTable.Rows.Add(0);
            return (double)(loDataTable.Rows[0]["Eval"]);
        }

        public static double MathChallenge(string expression)
        {
            return (double)new System.Xml.XPath.XPathDocument
            (new StringReader("<r/>")).CreateNavigator().Evaluate
            (string.Format("number({0})", new Regex
            (@"([\+\-\*])")
            .Replace(expression, " ${1} ")
            .Replace("/", " div ")
            .Replace("%", " mod ")));
        }

        static string JsonCleanup()
        {
            WebRequest request = WebRequest.Create("https://coderbyte.com/api/challenges/json/json-cleaning");
            WebResponse response = request.GetResponse();

            var jsonString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            string originalEmptyValues = "(\"\"|\"[-]\"|\"N\\\\/A\")";
            string emptyPlaceholder = "#empty#";
            string emptyKeyValues = "(\"[a-zA-Z]+\":#empty#,|,\"[a-zA-Z]+\":#empty#)";
            string emptyArrayValues = "(#empty#,|,#empty#)";
            string result = Regex.Replace(jsonString, originalEmptyValues, emptyPlaceholder);
            result = Regex.Replace(result, emptyKeyValues, "");
            result = Regex.Replace(result, emptyArrayValues, "");

            return result;
        }

        static void WebRequestAgeEqualOrGreaterThan50()
        {
            var list = new List<string>();
            WebRequest request = WebRequest.Create("https://coderbyte.com/api/challenges/json/age-counting");
            WebResponse response = request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            var formattedResponse = responseString.Replace("\"data\":", "").Replace("{\"", "").Replace("\"}", "");

            //var responseObj = JsonConvert.DeserializeObject<RootClass>(responseString);

            string[] array = formattedResponse.Split(",");

            foreach (var item in array)
            {
                if (item.Contains("age"))
                {
                    list.Add(item.ToLower().Trim().Replace("age=", ""));
                }
            }

            var result = list.Count(l => Convert.ToInt32(l) > 50);

            Console.WriteLine(result);
            response.Close();

        }

        public class Name
        {
            [JsonIgnore]
            public string first { get; set; }
            public string middle { get; set; }
            public string last { get; set; }
        }

        public class Education
        {
            public string highschool { get; set; }
            public string college { get; set; }
        }

        public class Root
        {
            public Name name { get; set; }
            public int age { get; set; }
            public string DOB { get; set; }
            public List<string> hobbies { get; set; }
            public Education education { get; set; }
        }

        public static void WebHttpRequest()
        {
            WebRequest request = WebRequest.Create("https://coderbyte.com/api/challenges/json/json-cleaning");
            WebResponse response = request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var resultObj = JsonConvert.DeserializeObject<Root>(responseString);

            var result = "";

            PropertyInfo[] properties = resultObj.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.GetValue(resultObj).GetType() == typeof(Name) ||
                    property.GetValue(resultObj).GetType() == typeof(Education))
                {
                    result += "{ " + property.GetValue(resultObj).GetType().Name + ": { ";
                    PropertyInfo[] typeProperties = property.GetValue(resultObj).GetType().GetProperties();

                    foreach (PropertyInfo typeProperty in typeProperties)
                    {
                        var value = typeProperty.GetValue(property.GetValue(resultObj));
                        if (value.ToString().Trim() != "N/A")
                        {
                            result += typeProperty.Name + ": " + value.ToString() + ", ";
                        }

                    }

                    result += "} }";
                }
                else
                {

                }

            }


            Console.WriteLine(response);
            response.Close();
        }

        static int NO_OF_CHARS = 256;
        static char[] count = new char[NO_OF_CHARS];

        static void getCharCountArray(string str)
        {
            for (int i = 0; i < str.Length; i++)
                count[str[i]]++;
        }

        static string FirstNonRepeatingCharacter(string str)
        {
            getCharCountArray(str);
            int index = -1, i;

            for (i = 0; i < str.Length; i++)
            {
                if (count[str[i]] == 1)
                {
                    index = i;
                    break;
                }
            }

            return str[index].ToString();
        }

        public static int[] RotateArray(int[] nums, int k)
        {
            var newArray = new List<int>();
            int count = nums.Length - k;
            for (int i = count; i < nums.Length; i++)
            {
                newArray.Add(nums[i]);
            }

            count = nums.Length - k;
            for (int i = 0; i < count; i++)
            {
                newArray.Add(nums[i]);
            }

            return newArray.ToArray();
        }

        public static List<string> GroupArray()
        {
            var original = new List<int> { 1, 2, 4, 6, 7, 8, 9, 10, 12, 17, 19, 20, 21, 24, 25, 30 };
            var grouped = new List<string>(); // { "1-2", "4", "6-10", "12", "17", "19-21", "24-25", "30" };

            for (int i = 0; i < original.Count; i++)
            {
                var result = FilterGroupList(original.Skip(i).ToList());
                if (result.Any())
                {
                    if (result.Count > 1)
                        grouped.Add($"{result[0].ToString()}-{result[result.Count - 1].ToString()}");
                    else
                        grouped.Add(result[0].ToString());

                    i += result.Count - 1;
                }

            }

            return grouped;
        }

        private static List<int> FilterGroupList(List<int> list)
        {
            var result = new List<int>();

            if (list.Count <= 0)
                return result;

            if (list.Count == 1)
            {
                result.Add(list[0]);
                return result;
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i + 1] - list[i] == 1)
                    result.Add(list[i]);
                else
                {
                    result.Add(list[i]);
                    return result;
                }
            }

            return result;
        }

        public static char FindTheDifference2(string s, string t)
        {
            char[] sortedS = s.ToCharArray();
            char[] sortedT = t.ToCharArray();
            Array.Sort(sortedS);
            Array.Sort(sortedT);
            for (int i = 0; i < s.Length; i++)
            {
                if (sortedS[i] != sortedT[i])
                {
                    return sortedT[i];
                }
            }

            return sortedT[s.Length];
        }

        public class TreeNode
        {
            public int Value { get; set; }
            public TreeNode Left { get; set; }
            public TreeNode Right { get; set; }
            public TreeNode(int Value) => this.Value = Value;
        }

        private static int MaxDepth(TreeNode root)
        {
            var depth = 1 + GetBinaryTreeDepth(root);
            return depth;
        }

        private static int GetBinaryTreeDepth(TreeNode root)
        {
            if (root == null)
                return -1;
            else
            {
                int leftDepth = GetBinaryTreeDepth(root.Left);
                int rightDepth = GetBinaryTreeDepth(root.Right);
                //Return max of left or right tree.
                return 1 + Math.Max(leftDepth, rightDepth);
            }
        }

        public static int CountElements(int[] array)
        {
            int count = 0;
            foreach (var ele in array)
            {
                foreach (var item in array)
                {
                    if (item == ele + 1)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public static int CalPoints(string[] ops)
        {
            List<int> list = new List<int> { };

            foreach (var op in ops)
            {
                int index = list.Count - 1 < 0 ? 0 : list.Count - 1;
                int length = list.Count;
                switch (op.ToUpper())
                {
                    case "C":
                        if (length > 0)
                            list.RemoveAt(index);
                        break;
                    case "D":
                        if (length > 0)
                            list.Add(list[index] * 2);
                        break;
                    case "+":
                        if (length > 0)
                            list.Add(list[index - 1] + list[index]);
                        break;
                    default:
                        list.Add(Convert.ToInt32(op));
                        break;
                }
            }

            int result = 0;
            foreach (var item in list)
                result += item;

            return result;
        }

        public static string fizzBuzz(int input)
        {
            bool fizz = input % 3 == 0;
            bool buzz = input % 5 == 0;

            if (fizz && buzz) return "FizzBuzz";

            if (fizz) return "Fizz";

            if (buzz) return "Buzz";

            return input.ToString();
        }

        public static string GetRecipient(string message, int position)
        {
            var userList = message.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var users = userList.Where(u => u.StartsWith("@"))
                .Select(u => Regex.Replace(u, @"[^\w\s]", "").Split(new string[] { " " }, StringSplitOptions.None)[0])
                .ToList();


            if (position > users.Count) return string.Empty;

            return users[position - 1];
        }

        public static int comparedate()
        {
            var today = DateTime.Now;
            var six_months = DateTime.Now.AddMonths(-6);

            LocalDate start = new LocalDate(today.Year, today.Month, today.Day);
            LocalDate end = new LocalDate(six_months.Year, six_months.Month, six_months.Day);
            Period period = Period.Between(start, end);
            int months = period.Months;

            return months;
        }

        public static string Numerals4(int num)
        {
            var roman_u = new Dictionary<int, string>
            {
                { 1, "I" },
                { 5, "V" },
                { 10, "X" },
                { 50, "L" },
                { 100, "C" },
                { 500, "D" },
                { 1000, "M" },
            };

            var result = "";
            string numString = num.ToString();
            int numCount = numString.Length;

            while (num > 0)
            {
                int range = Convert.ToInt32(num.ToString().Substring(0).PadRight(numCount, '0'));
                var compareRange = roman_u.LastOrDefault(r => r.Key <= range && r.Key <= num);
                var appearCount = roman_u.Count(r => r.Key == compareRange.Key);
                result += "".PadRight(appearCount, roman_u[compareRange.Key].ToCharArray()[0]);
                num -= compareRange.Key;
                numCount = num.ToString().Length;
            }

            return result;
        }

        public static string Numerals3(int num)
        {
            var roman = new Dictionary<int, string>
            {
                { 1, "I" },
                { 5, "v" },
                { 10, "X" },
                { 50, "L" },
                { 100, "C" },
                { 500, "D" },
                { 1000, "M" },
            };

            var roman_string = "";
            foreach (var key in roman.Keys.OrderByDescending(k => k))
            {
                while (num > key)
                {
                    roman_string += roman[key];
                    num = key;
                }
            }

            return roman_string;
        }

        public static string Numerals2(int num)
        {
            string numString = num.ToString();
            int numCount = numString.Length;
            string result = "";
            foreach (var item in numString)
            {
                string roman = "";
                int value = Convert.ToInt32(item.ToString());
                switch (numCount)
                {
                    case (> 3):
                        roman = "M";
                        break;
                    case (3):
                        if (value >= 1 && value <= 3)
                            roman = "C";
                        else if (value == 4)
                            roman = "CD";
                        else if (value >= 5 && value <= 8)
                            roman = "D";
                        else
                            roman = "CM";
                        break;
                    case (2):
                        if (value >= 1 && value <= 3)
                            roman = "X";
                        else if (value == 4)
                            roman = "XL";
                        else if (value >= 5 && value <= 8)
                            roman = "L";
                        else
                            roman = "XC";
                        break;
                    case (1):
                        if (value >= 1 && value <= 3)
                            roman = "I";
                        else if (value == 4)
                            roman = "IV";
                        else if (value >= 5 && value <= 8)
                            roman = "V";
                        else
                            roman = "IX";
                        break;

                    default:
                        break;
                }

                result += roman;

                numCount--;
            }

            return result;
        }

        public static string Numerals(int num)
        {
            var numerals = "";
            var roman = new Dictionary<string, int>
            {
                { "M", 1000 },
                { "CM", 900 },
                { "D", 500 },
                {"CD" , 400 },
                { "C", 100 },
                { "XC", 90 },
                { "L", 50 },
                { "XL", 40 },
                { "X", 10 },
                { "IX", 9 },
                { "V", 5 },
                { "IV", 4 },
                { "I",  1 }
            };

            foreach (var item in roman.Keys)
            {
                var q = (int)(Math.Floor(Convert.ToDecimal(num / roman[item])));
                num -= q * roman[item];
                numerals += item.PadRight(q);
            }
            return numerals;
        }

        public static int MaxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts)
        {
            Array.Sort(horizontalCuts);
            Array.Sort(verticalCuts);
            long hmax = 0;
            long wmax = 0;
            for (int i = 0; i <= horizontalCuts.Length; i++)
            {
                if (i == 0)
                {
                    hmax = horizontalCuts[i];
                }
                else if (i == horizontalCuts.Length)
                {
                    hmax = hmax > h - horizontalCuts[i - 1] ? hmax : h - horizontalCuts[i - 1];
                }
                else
                {
                    long diff = horizontalCuts[i] - horizontalCuts[i - 1];
                    hmax = hmax > diff ? hmax : diff;
                }
            }

            for (int i = 0; i <= verticalCuts.Length; i++)
            {
                if (i == 0)
                {
                    wmax = verticalCuts[i];
                }
                else if (i == verticalCuts.Length)
                {
                    wmax = wmax > w - verticalCuts[i - 1] ? wmax : w - verticalCuts[i - 1];
                }
                else
                {
                    long diff = verticalCuts[i] - verticalCuts[i - 1];
                    wmax = wmax > diff ? wmax : diff;
                }
            }

            long mod = 1000000007;
            return (int)(((wmax % mod) * (hmax % mod)) % mod);

        }

        public static Dictionary<String, int> sample(List<String> data)
        {
            Dictionary<String, Int32> result = new Dictionary<String, Int32>();
            for (Int32 i = 0; i < data.Count; i++)
            {
                if (result.ContainsKey(data[i]))
                {
                    Int32 value = 0;
                    result.TryGetValue(data[i], out value);
                    result[data[i]] = value;
                    var a = result[data[i]];
                    result.Remove(data[i]); // -- otherwise an exception on the next line?!?
                    result.Add(data[i], value + 1);
                }
                else
                    result.Add(data[i], 1);
            }
            return result;
        }

        public static Dictionary<string, int> FrequencyCount(IEnumerable<string> data)
        {
            var result = new Dictionary<string, int>();
            foreach (var word in data)
            {
                if (result.ContainsKey(word))
                {
                    result[word] = result[word] + 1;
                }
                else
                {
                    result[word] = 1;
                }
            }

            return result;
        }

        public static long storage(int n, int m, List<int> h, List<int> v)
        {
            h.OrderBy(a => a); // Time: O(n * log(n)) | Space: O(1) | n = horizontalCuts.length;
            v.OrderBy(a => a); // Time: O(m * log(m)) | Space: O(1) | m = verticalCuts.length;

            int maxHorizontal = GetMaxSegment(h); // Time: O(n) | Space: O(1)
            int maxVertical = GetMaxSegment(v); // Time: O(m) | Space: O(1)

            return (maxHorizontal * maxVertical); // O(1)
        }

        public static int GetMaxSegment(List<int> cuts)
        {
            int localMaxV = 1;
            int length = cuts.Count;
            for (int i = 0; i < length; i++)
            {
                if (i == 0 || cuts[i] - cuts[i - 1] == 1)
                {
                    localMaxV += 1;
                }
                else
                {
                    localMaxV = 2;
                }
            }
            return localMaxV;
        }
        #endregion
    }
}
