﻿using System;

namespace ConsoleApplication4
{


    class Program
    {
        static void Main(string[] args)
        {
            //B obj = new B();
            //obj.j = 2;
            //obj.display();
            //Console.ReadLine();
            // == > 2

            //sample1 obj = new sample1();
            //obj.i = 1;
            //obj.j = 2;
            //obj.display();
            //Console.ReadLine();
            //== > 2

            //B11 obj = new B11();
            //obj.i = 1;
            //obj.j = 2;
            //obj.display();
            //Console.ReadLine();
            // ==> Compile error

            F str1 = new F(sample2.fun);
            string str = str1("Test Your c#.NET skills");
            Console.WriteLine(str);
        }
    }


    abstract class A
    {
        int i;
        public abstract void display();
    }

    class B : A
    {
        public int j;
        public override void display()
        {
            Console.WriteLine(j);
        }
    }

    class sample
    {
        public int i;
        void display()
        {
            Console.WriteLine(i);
        }
    }

    class sample1 : sample
    {
        public int j;
        public void display()
        {
            Console.WriteLine(j);
        }
    }

    class A11
    {
        public int i;
        private int j;
    }

    class B11 : A11
    {
        //void display()
        //{
        //    base.j = base.i + 1;
        //    Console.WriteLine(base.i + " " + base.j);
        //}
    }

    delegate string F(string str);
    class sample2
    {
        public static string fun(string a)
        {
            return a.Replace(' ', '-');
        }
    }
}



