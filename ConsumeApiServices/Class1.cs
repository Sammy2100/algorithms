﻿using System;
using System.Linq;

namespace ConsumeApiServices
{
    public class WemaTest
    {
        public bool PerformanceTest(int[] input, int value)
        {
            int length = input.Length;

            for (int i = 0; i < length; i++)
            {
                if (input[i] == value)
                    return true;
            }

            return false;
        }

        public void PerformanceTest2(int[] input)
        {
            int length = input.Length;

            for (int i = 0; i < length; i++)
            {
                for (int j = i; j < length; j++)
                    Console.WriteLine();
            }
        }

        public void PerformanceTest3(int[] input, int valueIndex)
        {
            int length = input.Length;

            int result = input[valueIndex];

            Console.WriteLine(result);
        }

        public bool AlgorithmTest(string[] items, string valueToFind, int valueIndex)
        {
            // ===================================//
            foreach (var item in items)
            {
                if (item == valueToFind)
                    return true;
            }

            // ===================================//
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == valueToFind)
                    return true;
            }

            // ===================================//
            for (int i = 0; i < items.Length; i++)
            {
                for (int j = i; j < items.Length; j++)
                {
                    if (items[j] == valueToFind)
                        return true;
                }
            }

            // ===================================//
            var result = items.FirstOrDefault(i => i == valueToFind);
            if (result != null)
                return true;

            // ===================================//
            if (items[valueIndex] == valueToFind)
                return true;

            return false;
        }
    }
}
